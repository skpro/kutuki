package com.swaroop.kutuki.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.swaroop.kutuki.R
import com.swaroop.kutuki.adapters.VideoAdapter
import com.swaroop.kutuki.databinding.ActivityPreviewVideosBinding
import com.swaroop.kutuki.models.VideoData
import com.swaroop.kutuki.utils.ExoPlayerActivity
import com.swaroop.kutuki.viewModels.VideosViewModel

class PreviewVideosActivity : AppCompatActivity() {
    private var activityPreviewVideosActivity: ActivityPreviewVideosBinding? = null
    private var videosViewModel: VideosViewModel? = null
    private var videosAdapter: VideoAdapter? = null
    private var videosMap = HashMap<String, VideoData>()
    private var rvVideosList: RecyclerView? = null
    private var videoView: VideoView? = null
    private var category: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
    }

    private fun initialize() {
        category = intent.getStringExtra("category_name")
        videosViewModel = ViewModelProvider(this).get(VideosViewModel::class.java)
        activityPreviewVideosActivity =
            DataBindingUtil.setContentView(this, R.layout.activity_preview_videos)
        activityPreviewVideosActivity!!.lifecycleOwner = this
        videoView = activityPreviewVideosActivity!!.videoView
        activityPreviewVideosActivity!!.progressCircular.visibility = View.VISIBLE
        videosViewModel!!.getVideoObjects()!!.observe(this) {
            if (it != null) {
                if (it.code == 200) {
                    activityPreviewVideosActivity!!.llErrorLayout.visibility = View.GONE
                    videosMap = it.response.videos
                    initList()
                } else {
                    activityPreviewVideosActivity!!.llErrorLayout.visibility = View.VISIBLE
                }
            } else {
                activityPreviewVideosActivity!!.llErrorLayout.visibility = View.VISIBLE
            }
            activityPreviewVideosActivity!!.progressCircular.visibility = View.GONE
        }
        videosViewModel!!.getVideo()
        activityPreviewVideosActivity!!.btnBack.setOnClickListener {
            finish()
        }
    }

    private fun initList() {
        var lastOpened = 0
        val videoList = ArrayList<VideoData>()
        for ((_, value) in videosMap) {
            val far = value.categories.split(",")
            for (category in far) {
                if (this.category == category) {
                    videoList.add(
                        VideoData(
                            value.title,
                            value.description,
                            value.thumbnailURL,
                            value.videoURL,
                            value.categories
                        )
                    )
                }
            }

        }
        videoList[0].isSelected = true
        var url = videoList[0].videoURL
        var uri = Uri.parse(url)
        val mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)
        videoView!!.setMediaController(mediaController)
        videoView!!.setVideoURI(uri)
        videoView!!.start()
        videosAdapter = VideoAdapter(videoList, this) {
            url = videoList[it].videoURL
            uri = Uri.parse(url)
            mediaController.setAnchorView(videoView)
            videoView!!.setVideoURI(uri)
            videoView!!.start()
            if (!videoList[it].isSelected)
                videoList[it].isSelected = true
            videosAdapter?.notifyItemChanged(it)
            if (lastOpened != -1 && lastOpened != it) {
                if (videoList[lastOpened].isSelected) {
                    videoList[lastOpened].isSelected = false
                    videosAdapter?.notifyItemChanged(lastOpened)
                }
            }
            lastOpened = it
        }
        rvVideosList = activityPreviewVideosActivity!!.rvVideoList
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rvVideosList!!.layoutManager = layoutManager
        rvVideosList!!.adapter = videosAdapter
        activityPreviewVideosActivity!!.fullScreen.setOnClickListener {
            fullScreen(url)
        }
    }

    private fun fullScreen(url: String) {
        val intent = Intent(this, ExoPlayerActivity::class.java)
        intent.putExtra("intent_video_url", url)
        startActivity(intent)

    }

}