package com.swaroop.kutuki.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.swaroop.kutuki.R
import com.swaroop.kutuki.adapters.CategoryAdapter
import com.swaroop.kutuki.databinding.FragmentCategoryBinding
import com.swaroop.kutuki.models.CustomCategoryModel

class CategoryFragment : Fragment() {
    private var categoryAdapter: CategoryAdapter? = null
    private var fragmentCategoryBinding: FragmentCategoryBinding? = null
    private var rvCategoriesList: RecyclerView? = null
    private var categories = ArrayList<CustomCategoryModel>()

    companion object {
        fun newInstance(
            page: Int,
            videoCategories: ArrayList<CustomCategoryModel>
        ): CategoryFragment {
            val args = Bundle()
            args.putInt("page", page)
            args.putParcelableArrayList("category", videoCategories)
            val fragment = CategoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categories = arguments?.getParcelableArrayList("category")!!
        initList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentCategoryBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_category, container, false)
        return fragmentCategoryBinding!!.root
    }

    private fun initList() {
        rvCategoriesList = fragmentCategoryBinding!!.rvCategories
        categoryAdapter = CategoryAdapter(
            categories
        ) { categoryName ->
            val intent = Intent(activity, PreviewVideosActivity::class.java)
            intent.putExtra("category_name", categoryName)
            startActivity(intent)
        }
        rvCategoriesList!!.layoutManager =
            GridLayoutManager(activity, 4)
        rvCategoriesList!!.adapter = categoryAdapter
    }
}