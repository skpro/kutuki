package com.swaroop.kutuki.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.swaroop.kutuki.R
import com.swaroop.kutuki.adapters.ViewPagerAdapter
import com.swaroop.kutuki.databinding.ActivityCategoriesBinding
import com.swaroop.kutuki.models.CategoryData
import com.swaroop.kutuki.models.CustomCategoryModel
import com.swaroop.kutuki.viewModels.CategoriesViewModel


class CategoriesActivity : AppCompatActivity() {
    private var categoryViewModel: CategoriesViewModel? = null
    private var activityCategoryBinding: ActivityCategoriesBinding? = null
    private var videoCategories = HashMap<String, CategoryData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
    }

    private fun initialize() {
        categoryViewModel = ViewModelProvider(this).get(CategoriesViewModel::class.java)
        activityCategoryBinding = DataBindingUtil.setContentView(this, R.layout.activity_categories)
        activityCategoryBinding!!.lifecycleOwner = this
        activityCategoryBinding!!.progressCircular.visibility = View.VISIBLE
        categoryViewModel!!.getCategoryObjects()!!.observe(this) {
            if (it != null) {
                if (it.code == 200) {
                    activityCategoryBinding!!.llErrorLayout.visibility = View.GONE
                    videoCategories = it.response.videoCategories
                    initViewPage()
                } else {
                    activityCategoryBinding!!.llErrorLayout.visibility = View.VISIBLE
                }
            } else {
                activityCategoryBinding!!.llErrorLayout.visibility = View.VISIBLE
            }
            activityCategoryBinding!!.progressCircular.visibility = View.GONE
        }
        categoryViewModel!!.getCategories()
    }

    private fun initViewPage() {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        val categories = ArrayList<CustomCategoryModel>()
        for ((key, value) in videoCategories) {
            categories.add(
                CustomCategoryModel(
                    value.name.replace("Category", "").toInt(),
                    key,
                    value.name,
                    value.image
                )
            )
        }
        val sortedList = categories.sortedWith(compareBy { it.categoryNo })
        var min = 0
        var max = 8
        for (i in 0 until viewPagerSize(categories.size)) {
            val list = fillRange(min, max, ArrayList(sortedList))
            viewPagerAdapter.addFragment(
                CategoryFragment.newInstance(
                    i,
                    ArrayList(list)
                ), ""
            )
            min += 8
            max += 8
        }
        activityCategoryBinding!!.viewPager.adapter = viewPagerAdapter
        activityCategoryBinding!!.viewPager.currentItem = 0
        activityCategoryBinding!!.tabLayout.setupWithViewPager(
            activityCategoryBinding!!.viewPager,
            true
        )
    }

    private fun viewPagerSize(length: Int): Int {
        val result = length / 8
        return if (length % 8 == 0) {
            result
        } else {
            result + 1
        }
    }

    private fun fillRange(
        min: Int,
        max: Int,
        list: ArrayList<CustomCategoryModel>
    ): ArrayList<CustomCategoryModel> {
        val filteredList: ArrayList<CustomCategoryModel> = ArrayList()
        for (i in min until max) {
            if (i < list.size) {
                filteredList.add(list[i])
            }
        }
        return filteredList
    }
}