package com.swaroop.kutuki.models

import com.google.gson.annotations.SerializedName

class VideoData(
    @field:SerializedName("title") var title: String,
    @field:SerializedName(
        "description"
    ) var description: String,
    @field:SerializedName("thumbnailURL") var thumbnailURL: String,
    @field:SerializedName(
        "videoURL"
    ) var videoURL: String,
    @field:SerializedName("categories") var categories: String
) {
    var isSelected = false

}