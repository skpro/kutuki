package com.swaroop.kutuki.models

import android.os.Parcelable
import android.os.Parcel
import com.swaroop.kutuki.models.CustomCategoryModel

class CustomCategoryModel : Parcelable {
    var categoryId: String?
    var categoryName: String?
        private set
    var categoryImage: String?
        private set
    var categoryNo: Int

    protected constructor(`in`: Parcel) {
        categoryId = `in`.readString()
        categoryName = `in`.readString()
        categoryImage = `in`.readString()
        categoryNo = `in`.readInt()
    }

    constructor(
        categoryNo: Int,
        categoryId: String?,
        categoryName: String?,
        categoryImage: String?
    ) {
        this.categoryId = categoryId
        this.categoryName = categoryName
        this.categoryImage = categoryImage
        this.categoryNo = categoryNo
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(categoryId)
        parcel.writeString(categoryName)
        parcel.writeString(categoryImage)
    }
}