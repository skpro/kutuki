package com.swaroop.kutuki.models

import com.google.gson.annotations.SerializedName

class CategoryModel {
    @SerializedName("code")
    var code: Int? = null

    @SerializedName("response")
    var response: CategoryResponse? = null
}