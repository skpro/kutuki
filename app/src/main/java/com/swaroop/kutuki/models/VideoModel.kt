package com.swaroop.kutuki.models

import com.google.gson.annotations.SerializedName

class VideoModel {
    @SerializedName("code")
    var code: Int? = null

    @SerializedName("response")
    var response: VideoResponse? = null
}