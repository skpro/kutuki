package com.swaroop.kutuki.models

import com.google.gson.annotations.SerializedName

class CategoryData {
    @SerializedName("name")
    var name: String? = null

    @SerializedName("image")
    var image: String? = null
}