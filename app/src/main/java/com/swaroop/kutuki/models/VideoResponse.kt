package com.swaroop.kutuki.models

import com.google.gson.annotations.SerializedName
import java.util.HashMap

class VideoResponse {
    @SerializedName("videos")
    var videos: HashMap<String, VideoData>? = null
}