package com.swaroop.kutuki.models

import com.google.gson.annotations.SerializedName
import java.util.HashMap

class CategoryResponse {
    @SerializedName("videoCategories")
    var videoCategories: HashMap<String, CategoryData>? = null
}