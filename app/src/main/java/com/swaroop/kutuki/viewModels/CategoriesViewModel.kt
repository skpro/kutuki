package com.swaroop.kutuki.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.swaroop.kutuki.models.CategoryModel
import com.swaroop.kutuki.repos.CategoryRepo

class CategoriesViewModel : ViewModel() {
    private var categoryResponse: MutableLiveData<CategoryModel>? = null
    private val categoryRepo = CategoryRepo()
    fun getCategoryObjects(): MutableLiveData<CategoryModel>? {
        if (categoryResponse == null) {
            categoryResponse = categoryRepo.getObject()
        }
        return categoryResponse
    }

    fun getCategories() {
        categoryResponse = categoryRepo.getCategories()
    }
}