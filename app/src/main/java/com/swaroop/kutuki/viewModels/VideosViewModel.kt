package com.swaroop.kutuki.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.swaroop.kutuki.models.VideoModel
import com.swaroop.kutuki.repos.VideoRepo

class VideosViewModel : ViewModel() {
    private var videoResponse: MutableLiveData<VideoModel>? = null
    private val videoRepo = VideoRepo()
    fun getVideoObjects(): MutableLiveData<VideoModel>? {
        if (videoResponse == null) {
            videoResponse = videoRepo.getObject()
        }
        return videoResponse
    }

    fun getVideo() {
        videoResponse = videoRepo.getVideos()
    }
}