package com.swaroop.kutuki.repos

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.swaroop.kutuki.models.CategoryModel
import com.swaroop.kutuki.utils.RetrofitClientInstance
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class CategoryRepo {
    var categoryResponse: MutableLiveData<CategoryModel> = MutableLiveData()

    fun getObject(): MutableLiveData<CategoryModel> {
        return categoryResponse
    }

    fun getCategories(): MutableLiveData<CategoryModel> {
        val service = RetrofitClientInstance.retrofit.create(
            RetrofitClientInstance::class.java
        )

        service.getCategories().observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<CategoryModel> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onSuccess(t: CategoryModel) {
                    Log.d("Success", "${t.code}")
                    categoryResponse.postValue(t)
                }

                override fun onError(e: Throwable) {
                    categoryResponse.postValue(null)
                }
            })

        return categoryResponse
    }
}