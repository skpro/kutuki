package com.swaroop.kutuki.repos

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.swaroop.kutuki.models.VideoModel
import com.swaroop.kutuki.utils.RetrofitClientInstance
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class VideoRepo {
    var videoResponse: MutableLiveData<VideoModel> = MutableLiveData()

    fun getObject(): MutableLiveData<VideoModel> {
        return videoResponse
    }

    fun getVideos(): MutableLiveData<VideoModel> {
        val service = RetrofitClientInstance.retrofit.create(
            RetrofitClientInstance::class.java
        )

        service.getVideos().observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<VideoModel> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onSuccess(t: VideoModel) {
                    Log.d("Success", "${t.code}")
                    videoResponse.postValue(t)
                }

                override fun onError(e: Throwable) {
                    videoResponse.postValue(null)
                }
            })

        return videoResponse
    }
}