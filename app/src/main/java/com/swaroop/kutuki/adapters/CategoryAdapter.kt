package com.swaroop.kutuki.adapters

import com.swaroop.kutuki.models.CustomCategoryModel
import com.swaroop.kutuki.adapters.CategoryAdapter.CategoryViewModel
import android.view.ViewGroup
import android.view.LayoutInflater
import com.swaroop.kutuki.R
import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.swaroop.kutuki.utils.KutukiUtils
import java.util.ArrayList

class CategoryAdapter(
    private val videoCategories: ArrayList<CustomCategoryModel>,
    private var onClickListener: OnClickListener
) : RecyclerView.Adapter<CategoryViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewModel {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.category_list_item, parent, false)
        return CategoryViewModel(view)
    }

    override fun onBindViewHolder(
        holder: CategoryViewModel,
        @SuppressLint("RecyclerView") position: Int
    ) {
        KutukiUtils.loadRoundedImage(
            videoCategories[position].categoryImage,
            holder.ivCategoryImage
        )
        holder.ivCategoryImage.setOnClickListener {
            onClickListener.onClick(
                videoCategories[position].categoryName
            )
        }
    }

    override fun getItemCount(): Int {
        return videoCategories.size
    }

    class CategoryViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivCategoryImage: ImageView = itemView.findViewById(R.id.iv_category_image)

    }

    interface OnClickListener {
        fun onClick(categoryName: String?)
    }
}