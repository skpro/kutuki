package com.swaroop.kutuki.adapters

import com.swaroop.kutuki.models.VideoData
import com.swaroop.kutuki.adapters.VideoAdapter.VideoViewModel
import android.view.ViewGroup
import android.view.LayoutInflater
import com.swaroop.kutuki.R
import android.annotation.SuppressLint
import android.content.Context
import com.swaroop.kutuki.utils.KutukiUtils
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class VideoAdapter(
    private val videoCategories: ArrayList<VideoData>,
    var context: Context,
    private var onClickListener: OnClickListener
) : RecyclerView.Adapter<VideoViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewModel {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.thumbnail_list_item, parent, false)
        return VideoViewModel(view)
    }

    override fun onBindViewHolder(
        holder: VideoViewModel,
        @SuppressLint("RecyclerView") position: Int
    ) {
        if (videoCategories[position].thumbnailURL != null) {
            KutukiUtils.loadImage(videoCategories[position].thumbnailURL, holder.ivThumbnailImage)
        }
        if (videoCategories[position].isSelected) {
            val width = pxSize(192).toInt()
            val height = pxSize(102).toInt()
            holder.llBorder.layoutParams.width = width
            holder.llBorder.layoutParams.height = height
            holder.ivThumbnailImage.layoutParams.width = width
            holder.ivThumbnailImage.layoutParams.height = height
        } else {
            val width = pxSize(186).toInt()
            val height = pxSize(86).toInt()
            holder.llBorder.layoutParams.width = width
            holder.llBorder.layoutParams.height = pxSize(86).toInt()
            holder.ivThumbnailImage.layoutParams.width = width
            holder.ivThumbnailImage.layoutParams.height = height
        }
        holder.ivThumbnailImage.setOnClickListener { onClickListener.onClick(position) }
    }

    private fun pxSize(size: Int): Float {
        val r = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            size.toFloat(),
            r.displayMetrics
        )
    }

    override fun getItemCount(): Int {
        return videoCategories.size
    }

    class VideoViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivThumbnailImage: ImageView = itemView.findViewById(R.id.iv_thumbnail_image)
        var llBorder: CardView = itemView.findViewById(R.id.ll_border)

    }

    interface OnClickListener {
        fun onClick(position: Int)
    }
}