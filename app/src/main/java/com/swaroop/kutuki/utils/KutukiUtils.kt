package com.swaroop.kutuki.utils

import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation

object KutukiUtils {
    fun loadImage(url: String?, imageView: ImageView?) {
        Picasso.get().load(url).into(imageView)
    }

    fun loadRoundedImage(url: String?, imageView: ImageView?) {
        val transformation: Transformation = RoundedCornersTransform(500, 0)
        Picasso.get().load(url).transform(transformation).into(imageView)
    }
}