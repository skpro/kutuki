package com.swaroop.kutuki.utils

import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.SimpleExoPlayer
import android.widget.LinearLayout
import android.os.Bundle
import com.swaroop.kutuki.R
import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.util.Util

class ExoPlayerActivity : AppCompatActivity() {
    private var INTENT_VIDEO_URL = "intent_video_url"
    private var playerView: PlayerView? = null
    private var player: SimpleExoPlayer? = null
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0
    private var mediaUrl: String? = null
    private var progressBar: ProgressBar? = null
    private var appBar: LinearLayout? = null
    private var playbackStateListener: PlaybackStateListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exo_player)
        progressBar = findViewById(R.id.progressbar)
        appBar = findViewById(R.id.app_bar)
        progressBar!!.visibility = View.VISIBLE
        playerView = findViewById(R.id.video_view)
        playbackStateListener = PlaybackStateListener()
        mediaUrl = intent.getStringExtra(INTENT_VIDEO_URL)
        val ivBack = findViewById<ImageView>(R.id.iv_back)
        ivBack.setOnClickListener { view: View? -> finish() }
    }

    private fun initializePlayer() {
        player = SimpleExoPlayer.Builder(this).build()
        playerView!!.player = player
        val mediaItem = MediaItem.fromUri(
            mediaUrl!!
        )
        player!!.setMediaItem(mediaItem)
        player!!.playWhenReady = playWhenReady
        player!!.seekTo(currentWindow, playbackPosition)
        player!!.addListener(playbackStateListener!!)
        player!!.prepare()
    }

    public override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) {
            initializePlayer()
        }
    }

    public override fun onResume() {
        super.onResume()
        hideSystemUi()
        if (Util.SDK_INT < 24 || player == null) {
            initializePlayer()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (Util.SDK_INT < 24) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT >= 24) {
            releasePlayer()
        }
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        playerView!!.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    private fun releasePlayer() {
        if (player != null) {
            playWhenReady = player!!.playWhenReady
            playbackPosition = player!!.currentPosition
            currentWindow = player!!.currentWindowIndex
            player!!.removeListener((playbackStateListener as Player.Listener?)!!)
            player!!.release()
            player = null
        }
    }

    private inner class PlaybackStateListener : Player.Listener {
        override fun onPlaybackStateChanged(playbackState: Int) {
            when (playbackState) {
                ExoPlayer.STATE_BUFFERING -> {
                    progressBar!!.visibility = View.VISIBLE
                    appBar!!.visibility = View.GONE
                }
                ExoPlayer.STATE_IDLE, ExoPlayer.STATE_READY, ExoPlayer.STATE_ENDED -> {
                    progressBar!!.visibility = View.GONE
                    appBar!!.visibility = View.VISIBLE
                }
                else -> {
                    progressBar!!.visibility = View.GONE
                    appBar!!.visibility = View.VISIBLE
                }
            }
        }
    }
}